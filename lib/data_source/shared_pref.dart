import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  Future<double> getHscore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    double hscore = prefs.getDouble('hscore') ?? 0;
    return hscore;
  }

  Future<double> getLscore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double lscore = prefs.getDouble('lscore') ?? 0;
    return lscore;
  }

  setScore(double score) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double lscore = prefs.getDouble('lscore') ?? 0;
    double hscore = prefs.getDouble('hscore') ?? 0;
    if (hscore < score) {
      prefs.setDouble('hscore', score);
    }
    prefs.setDouble('lscore', score);
  }
}
