class Resources {
  static final List<String> photosChina = [
    "assets/images/people/china/1.jpg",
    "assets/images/people/china/2.jpg",
    "assets/images/people/china/3.jpg",
    "assets/images/people/china/4.jpg"
  ];
  static final List<String> photosJap = [
    "assets/images/people/jap/1.jpg",
  ];
  static final List<String> photosKor = [
    "assets/images/people/kor/1.jpg",
  ];
  static final List<String> photosThai = [
    "assets/images/people/thai/1.jpg",
    "assets/images/people/thai/2.jpg",
  ];
}
