import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:simplegame/util/configs.dart';


import 'storage_client.dart';

class StorageClientImpl implements StorageClient {
  Future<String> getValueFromSharedPref(String key) async {
    try {
      if (!Hive.isBoxOpen(HIVE_BOX)) await Hive.openBox(HIVE_BOX);
      var box = Hive.box(HIVE_BOX);
      return box.get(key);
    } catch (error, stackTrace) {
      debugPrint('error in get value from Storage $error , $stackTrace');
      return error.toString();
    }
  }

  Future<void> setValueToSharedPref(String key, String value) async {
    try {
      if (!Hive.isBoxOpen(HIVE_BOX)) await Hive.openBox(HIVE_BOX);
      var box = Hive.box(HIVE_BOX);
      box.put(key, value);
    } catch (error, stackTrace) {
      debugPrint('error in set value to Storage $error , $stackTrace');

    }
  }

  Future<void> clearSharedPref() async {
    if (!Hive.isBoxOpen(HIVE_BOX)) await Hive.openBox(HIVE_BOX);
    var box = Hive.box(HIVE_BOX);
    box.clear();
  }
}
