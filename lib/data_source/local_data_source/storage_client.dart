abstract class StorageClient{
  Future<String> getValueFromSharedPref(String key);
  setValueToSharedPref(String key, String value) ;
  clearSharedPref();
}