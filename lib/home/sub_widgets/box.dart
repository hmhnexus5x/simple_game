import 'dart:math';

import 'package:flutter/material.dart';
import 'package:simplegame/data_source/local_data_source/res.dart';

class DragBox extends StatefulWidget {
  final Offset initPos;
  final String label;
  final Color itemColor;
  final Offset mid;
  final ValueChanged<bool> changePoint;
  final Function countRound;

  DragBox(this.initPos, this.label, this.itemColor, this.mid, this.changePoint,
      this.countRound);

  @override
  DragBoxState createState() => DragBoxState();
}

class DragBoxState extends State<DragBox> with TickerProviderStateMixin {
  Offset position = Offset(0.0, 0.0);
  late Animation _boxAnimationX;
  late Animation _boxAnimationY;
  late Animation _boxAnimationVer;
  late AnimationController _boxAnimationController;
  late AnimationController _boxAnimationVerController;
  late List<String> imgPath;
  late Animation _boxAnimationSize;
  bool isLocated = false;

  List<String> selectPhoto() {
    Random random = new Random();
    int randomNumber = random.nextInt(4);
    switch (randomNumber) {
      case 0:
        int rn = random.nextInt(4);
        return [Resources.photosChina[rn], "china"];
        break;
      case 1:
        return [Resources.photosJap[0], "jap"];
        break;
      case 2:
        return [Resources.photosKor[0], "kor"];
        break;
      case 3:
        int rn = random.nextInt(2);
        return [Resources.photosThai[rn], "thai"];
        break;
      default:
        return ["", ""];
    }
    return ["", ""];
  }

  @override
  void initState() {
    super.initState();
    imgPath = selectPhoto();
    position = widget.initPos;
    _boxAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _boxAnimationVerController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 3000));
    _boxAnimationVer =
        Tween(begin: 0.0, end: 600.5).animate(_boxAnimationVerController);
    _boxAnimationSize =
        Tween(begin: 100.0, end: 0.0).animate(_boxAnimationController);
    _boxAnimationVerController.forward();
    _boxAnimationVerController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        imgPath = selectPhoto();
        _boxAnimationController.reset();
        _boxAnimationVerController.reset();
        isLocated = false;
        _boxAnimationVerController.forward();
        widget.countRound();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _boxAnimationVerController,
        builder: (contex, child1) {
          return AnimatedBuilder(
              animation: _boxAnimationController,
              builder: (context, child) {
                return Positioned(
                    left: isLocated == false
                        ? MediaQuery.of(context).size.width / 2 - 50
                        : _boxAnimationX.value,
                    top: isLocated == false
                        ? _boxAnimationVer.value
                        : _boxAnimationY.value,
                    child: Draggable(
                      data: widget.itemColor,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: Image.asset(imgPath[0],
                            height: _boxAnimationSize.value,
                            width: _boxAnimationSize.value,
                            fit: BoxFit.fill),
                      ),
                      onDraggableCanceled: (velocity, offset) {
                        setState(() {
                          position = offset;
                        });
                      },
                      childWhenDragging: Container(),
                      onDragEnd: (value) {
                        onDragEvents(value);
                      },
                      feedback: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: Image.asset(imgPath[0],
                            height: 100.0, width: 100.0, fit: BoxFit.fill),
                      ),
                    ));
              });
        });
  }

  onDragEvents(value) {
    if (value.offset.dx > widget.mid.dx && value.offset.dy > widget.mid.dy) {
      if (imgPath[1] == "thai") {
        setState(() {
          widget.changePoint(true);
        });
      } else {
        setState(() {
          widget.changePoint(false);
        });
      }
      _boxAnimationX = Tween(
              begin: value.offset.dx,
              end: MediaQuery.of(context).size.width - 100)
          .animate(_boxAnimationController);
      _boxAnimationY = Tween(
              begin: value.offset.dy,
              end: MediaQuery.of(context).size.height - 100)
          .animate(_boxAnimationController);
      setState(() {
        _boxAnimationController.reset();
        isLocated = true;
        _boxAnimationController.forward();
      });

      print("right bot");
    } else if (value.offset.dx < widget.mid.dx - 20 &&
        value.offset.dy < widget.mid.dy - 20) {
      if (imgPath[1] == "china") {
        setState(() {
          widget.changePoint(true);
        });
      } else {
        setState(() {
          widget.changePoint(false);
        });
      }
      _boxAnimationX = Tween(begin: value.offset.dx, end: 0.0)
          .animate(_boxAnimationController);
      _boxAnimationY = Tween(begin: value.offset.dy, end: 0.0)
          .animate(_boxAnimationController);
      setState(() {
        _boxAnimationController.reset();
        isLocated = true;
        _boxAnimationController.forward();
      });
      print("left top");
    } else if (value.offset.dx < widget.mid.dx - 20 &&
        value.offset.dy > widget.mid.dy + 20) {
      if (imgPath[1] == "jap") {
        setState(() {
          widget.changePoint(true);
        });
      } else {
        setState(() {
          widget.changePoint(false);
        });
      }
      _boxAnimationX = Tween(begin: value.offset.dx, end: 0.0)
          .animate(_boxAnimationController);
      _boxAnimationY = Tween(
              begin: value.offset.dy,
              end: MediaQuery.of(context).size.height - 100)
          .animate(_boxAnimationController);
      setState(() {
        _boxAnimationController.reset();
        isLocated = true;
        _boxAnimationController.forward();
      });

      print("left bot");
    } else if (value.offset.dx > widget.mid.dx &&
        value.offset.dy < widget.mid.dy) {
      if (imgPath[1] == "kor") {
        setState(() {
          widget.changePoint(true);
        });
      } else {
        setState(() {
          widget.changePoint(false);
        });
      }
      _boxAnimationX = Tween(
              begin: value.offset.dx,
              end: MediaQuery.of(context).size.width - 100)
          .animate(_boxAnimationController);
      _boxAnimationY = Tween(begin: value.offset.dy, end: 0.0)
          .animate(_boxAnimationController);
      setState(() {
        _boxAnimationController.reset();
        isLocated = true;
        _boxAnimationController.forward();
      });

      print("right top");
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _boxAnimationController.stop();
    _boxAnimationVerController.stop();
    super.dispose();
  }
}
