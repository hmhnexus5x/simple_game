import 'package:flutter/material.dart';

class FlagBox extends StatelessWidget {
  final String name;
  final String path;
  const FlagBox({Key? key, required this.name, required this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: Image.asset(path,
            height: 70.0, width: 100.0, fit: BoxFit.fill),
      ),
    );
  }
}
