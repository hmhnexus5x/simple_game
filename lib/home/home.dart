import 'package:flutter/material.dart';
import 'package:simplegame/home/sub_widgets/flag_box.dart';
import 'package:simplegame/welcome/welcome.dart';

import 'sub_widgets/box.dart';

class HomeScreen extends StatefulWidget {
  static const route = '/home_page';

  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  double x = 0;
  double y = 0;
  int totalPoint = 0;
  int round = 0;

  @override
  Widget build(BuildContext context) {
    y = MediaQuery.of(context).size.height / 2;
    x = MediaQuery.of(context).size.width / 2;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white.withOpacity(0.9),
        body: Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FlagBox(
                        name: "china", path: "assets/images/flag/china.png"),
                    FlagBox(name: "japan", path: "assets/images/flag/jap.png"),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FlagBox(name: "kor", path: "assets/images/flag/kor.png"),
                    FlagBox(name: "thai", path: "assets/images/flag/Thai.png"),
                  ],
                )
              ],
            ),
            Positioned(
                top: y,
                child: Row(
                  children: [
                    Text(" TotalPoints : "),
                    Text(totalPoint.toString())
                  ],
                )),
            DragBox(Offset(0.0, 0.0), 'Box One', Colors.blueAccent,
                Offset(x, y), changePoint,countRound),
          ],
        ),
      ),
    );
  }

  countRound() {
    round = round + 1;
    if (round > 10) {
      onPlayAgainTap(context);
    }
  }

  void onPlayAgainTap(BuildContext context) {
    Navigator.pushReplacementNamed(context, WelcomeScreen.route);
  }

  changePoint(bool t) {
    if (t) {
      setState(() {
        totalPoint = totalPoint + 20;
      });
    } else {
      setState(() {
        totalPoint = totalPoint - 5;
      });
    }
  }
}
