import 'package:flutter/material.dart';
import 'package:simplegame/home/home.dart';
import 'package:simplegame/res/costum_colors.dart';

class WelcomeScreen extends StatelessWidget {
  static const route = '/welcome_page';

  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Simple Game"),
      ),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.only(left: 20, top: 20),
            height: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  width: 2,
                ),
                color: Theme.of(context).primaryColor),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Highest score  :",
                      style: Theme.of(context).textTheme.headline2,
                    ),
                    SizedBox(width: 20,),
                    Text(
                      "0",
                      style: Theme.of(context).textTheme.headline3,
                    )
                  ],
                ),
                Row(
                  children: [
                    Text(
                      "Last score  :",
                      style: Theme.of(context).textTheme.headline2,
                    ),
                    SizedBox(width: 20,),

                    Text(
                      "0",
                      style: Theme.of(context).textTheme.headline3,
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(40),
            child: ElevatedButton(
              onPressed: ()=> onEnterAppTap(context),
              style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0))),
                padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                  EdgeInsets.zero,
                ),
              ),
              child: Ink(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                ),
                decoration: BoxDecoration(
                  gradient: PlayGradient,
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                ),
                child: Center(
                  child: Text(
                    "Play",
                    style: Theme.of(context).textTheme.bodyText2!.copyWith(
                          color: Colors.white,
                        ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  void onEnterAppTap(BuildContext context){
    Navigator.pushReplacementNamed(context, HomeScreen.route);
  }
}
