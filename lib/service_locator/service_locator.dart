import 'package:get_it/get_it.dart';
import 'package:simplegame/data_source/local_data_source/storage_client.dart';
import 'package:simplegame/data_source/local_data_source/storage_client_impl.dart';

GetIt getIt = GetIt.I;

setupServiceLocator() {
  getIt.registerSingleton<StorageClient>(StorageClientImpl());
}
