import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:simplegame/home/home.dart';
import 'package:simplegame/service_locator/service_locator.dart';
import 'package:simplegame/splash/splash.dart';
import 'package:simplegame/welcome/welcome.dart';

void main() {
  Hive.initFlutter();

  setupServiceLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simple Game',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Color(0xff344364),
        cardColor: Color(0xffF4F7FF),
        focusColor: Color(0xffD8E3FF),
        primaryColorDark: Color(0xff051436),
        primaryColorLight: Color(0xffC1C1C1),
        accentColor: Color(0xffEEC81B),
        highlightColor: Color(0xffEBBA19),
        shadowColor: Color(0xffFFD631).withOpacity(0.3),
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline2: TextStyle(
              fontSize: 29.0, fontWeight: FontWeight.bold, color: Colors.white),
          headline3: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.normal,
              color: Colors.white),
          headline4: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.normal,
              color: Color(0xff344364)),
          headline5: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
          headline6: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              color: Color(0xff344364)),
          bodyText2: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.normal,
              color: Color(0xff344364)),
          bodyText1: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.normal,
              color: Color(0xff344364)),
          subtitle1: TextStyle(fontSize: 12.0, color: Color(0xff344364)),
          subtitle2: TextStyle(fontSize: 10.0, color: Color(0xff344364)),
        ),
      ),
      initialRoute: SplashScreen.route,
      routes: {
        HomeScreen.route: (context) => HomeScreen(),
        WelcomeScreen.route: (context) => WelcomeScreen(),
        SplashScreen.route: (context) => SplashScreen(),
      },
    );
  }
}
