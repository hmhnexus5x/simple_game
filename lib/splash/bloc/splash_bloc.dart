import 'package:simplegame/data_source/local_data_source/storage_client.dart';
import 'package:simplegame/service_locator/service_locator.dart';

class SplashBloc {
  StorageClient storageClient = getIt<StorageClient>();

  Future<bool> getSession() async {
    try {
      String hscore = await storageClient.getValueFromSharedPref("hscore");
      String lscore = await storageClient.getValueFromSharedPref("lscore");
      if (hscore != null && lscore != null) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }
}
