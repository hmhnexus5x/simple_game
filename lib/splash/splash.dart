import 'dart:async';

import 'package:flutter/material.dart';
import 'package:simplegame/welcome/welcome.dart';

import 'bloc/splash_bloc.dart';


class SplashScreen extends StatefulWidget {
  static const route = '/splash';

  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late SplashBloc splashBloc;
  @override
  void initState() {
    splashBloc = SplashBloc();
    checkSession();

    super.initState();
  }

  checkSession() async {
    bool ck = await splashBloc.getSession();
    if (ck) {
      Timer(Duration(seconds: 3), () {
        Navigator.pushReplacementNamed(context, WelcomeScreen.route);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient:
            RadialGradient(colors: [Color(0xff323453), Color(0xff121432)])),
        child: Center(
          child: FlutterLogo(size: 100,),
          // child: Image.asset(
          //   "assets/images/logo.png",
          //   height: MediaQuery.of(context).size.height * 0.2,
          // ),
        ),
      ),
    );
  }
}
